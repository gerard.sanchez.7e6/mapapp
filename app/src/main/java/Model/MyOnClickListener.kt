package Model

interface MyOnClickListener {
    fun onMap(marker: Marker)
    fun delete(marker: Marker)
}