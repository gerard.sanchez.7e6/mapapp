package View

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.findNavController
import com.example.damaps.R
import com.example.damaps.databinding.FragmentRegisterBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore


class RegisterFragment : Fragment() {

    lateinit var binding: FragmentRegisterBinding
    private val db = FirebaseFirestore.getInstance()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentRegisterBinding.inflate(layoutInflater)
        (requireActivity() as AppCompatActivity).supportActionBar?.hide()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {3
        super.onViewCreated(view, savedInstanceState)
        binding.login.setOnClickListener{
            findNavController().navigate(R.id.action_registerFragment_to_loginFragment)
        }

        binding.signupButton.setOnClickListener {
            if (binding.usernameEdittext.text.toString()
                    .isNotEmpty() && binding.passwordEdittext.text.toString()
                    .isNotEmpty() && binding.emailEdittext.text.toString().isNotEmpty()
            ) {
                FirebaseAuth.getInstance().createUserWithEmailAndPassword(
                    binding.emailEdittext.text.toString(),
                    binding.passwordEdittext.text.toString()
                )
                    .addOnCompleteListener {
                        if (it.isSuccessful) {
                            val emailLogged = it.result?.user?.email
                            db.collection("users").document(emailLogged!!).set(
                                hashMapOf("name" to binding.usernameEdittext.text.toString(),)
                            )
                            val action =
                                RegisterFragmentDirections.actionRegisterFragmentToMapFragment(
                                    emailLogged
                                )
                            findNavController().navigate(action)
                        } else {
                            Toast.makeText(
                                requireContext(),
                                "User registration error",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
            }else{
                Toast.makeText(
                    requireContext(),
                    "All fields are required",
                    Toast.LENGTH_SHORT
                ).show()

            }
        }

    }

}